<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S02 Activity Code</title>
</head>
<body>
    <h2>Number Divisible By Five</h2>
    <p><?php printDivisibleOfFive(); ?></p>

    <h2>Adding - Counting - Removing student's name array lists</h2>
    <p><?php array_push($students, "John Smith"); ?></p>
    <p><?php var_dump($students); ?></p>
    <p><?php echo count($students); ?></p>
    <p><?php array_push($students, "Jane Smith"); ?></p>
    <p><?php var_dump($students); ?></p>
    <p><?php echo count($students); ?></p>
    <p><?php array_shift($students); ?></p>
    <p><?php var_dump($students); ?></p>
    <p><?php echo count($students); ?></p>


</body>
</html>