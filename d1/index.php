<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S02: Repetition Control Structures and Array Manipulation</title>
</head>
<body>
    <h1>Repetition Control Structures</h1>
    
    <h2>While Loop</h2>
    <p><?php whileLoop(); ?></p>

    <h2>Do While Loop</h2>
    <p><?php doWhileLoop(); ?></p>

    <h2>For Loop</h2>
    <p><?php forLoop(); ?></p>

    <h2>Continue and Break</h2>
    <p><?php modifiedForLoop(); ?></p>

    <h1>Array Manipulation</h1>

    <h2>2 Types of Array</h2>

    <h3>1: Simple Array</h3>
    <p><?php var_dump($computerBrands); ?></p>
    <!-- 
        foreach
            - This loop only works on array
            - loop through each key/value pair in an array.

        SYNTAX: (Simple Array)
            foreach($array as $element) {
                //code block to be executed.
            }
     -->

     <ul>
        <!-- php codes/statements can be breakdown using the php tags if it is incorporated with html tags. -->
        <?php foreach($computerBrands as $brand) { ?> 
            <!-- PHP includes a short hand method for "php echo" tag -->
        <li><?= $brand; ?></li>
        <?php } ?>
     </ul>

     <h3>2. Associative Array</h3>
     <!-- 
        foreach SYNTAX(associative array):
            foreach($array as $key => $value){
                // code to be executed
            }
      -->
      <ul>
        <?php foreach($gradesPeriods as $period => $grade) {?>
            <li>
                Grade in <?= $period?> is <?= $grade?>
            </li>
            <?php } ?>
      </ul>

      <h3>Multidimensional Array</h3>
      <ul>
        <?php
            // Each $heroes will be represented by a $team (accessing the outer array)
            foreach($heroes as $team) {
                // Each $team will be represented as $member (accessing the inner array)
                foreach($team as $member) {
        ?>
        <li><?= $member ?></li>
        <?php
                }
            }
        ?>
      </ul>

      <h3>Two-Dimensional Associative Array</h3>
      <ul>
        <?php
            foreach($ironManPowers as $label => $powerGroup) {
                foreach($powerGroup as $power) {
        ?>
       <li><?= "$label: $power" ?></li>

       <?php
                }
                
            }
        ?>
      </ul>

      <h2>Array Functions</h2>

      <h3>Original Array</h3>
      <pre><?php print_r($computerBrands); ?></pre>

      <h3>Sorted Array</h3>
      <h4>Sorting in Ascending Order</h4>
      <pre><?php print_r($sortedBrands); ?></pre>

      <h4>Sorting in Descending Order</h4>
      <pre><?php print_r($reverseSortedBrands); ?></pre>

      <h3>Appending element to array</h3>
      <h4>Add one or more elements at the end of an array</h4>
      <?php array_push($computerBrands, "Apple"); ?>
      <pre><?php print_r($computerBrands); ?></pre>

      <h4>Add one or more elements in the beginning of an array</h4>
      <?php array_unshift($computerBrands, "Dell"); ?>
      <pre><?php print_r($computerBrands); ?></pre>

      <h3>Removing element to array</h3>
      <h4>Removing element at the end of an array</h4>
      <?php array_pop($computerBrands); ?>
      <pre><?php print_r($computerBrands); ?></pre>

      <h4>Removing element in the beginning of an array</h4>
      <?php array_shift($computerBrands); ?>
      <pre><?php print_r($computerBrands); ?></pre>

      <h3>Others</h3>
      <h4>Counts the number of elements in an Array or equivalent to 'length' in javascript </h4> 
      <pre><?php echo count($computerBrands); ?></pre>

      <h4>in_array(): is used to check if the specific element exists in the array</h4>
      <p><?php echo searchBrands($computerBrands, "HP"); ?></p>
      <p><?php echo searchBrands($computerBrands, "Asus"); ?></p>

      <h4>array_reverse: returns the array in the reversed order.</h4>
      <pre><?php print_r($gradesPeriods); ?></pre>
      <pre><?php print_r($reverseGradePeriods); ?></pre>

</body>
</html>